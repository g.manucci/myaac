<?php

require '../../common.php';
require SYSTEM . 'functions.php';
require SYSTEM . 'init.php';
require SYSTEM . 'login.php';

$current_session = getSession('account');
if($current_session !== false)
{
	$account_logged = new OTS_Account();
	$account_logged->load($current_session);
	if($account_logged->isLoaded() && $account_logged->getPassword() == getSession('password')
		//&& (!isset($_SESSION['admin']) || admin())
		&& (getSession('remember_me') !== false || getSession('last_visit') > time() - 15 * 60)) {  // login for 15 minutes if "remember me" is not used
			$logged = true;

            //echo "encoded scope: " . $scope;
            $state = hash('sha256', time());
            $scope = urlencode("user:read:subscriptions");

            $account_logged->setCustomField('twitch_state', $state);
            
            //echo "encoded scope: " . $scope;
            echo "redirecting..." ;
            
            $redirect_url = "https://id.twitch.tv/oauth2/authorize?response_type=token&client_id=kc12r81fcalrx9mngd44fw6oz1vfqi&redirect_uri=http://localhost/tools/twitch/auth.php&state=$state&scope=$scope&force_verify=true";

}
else {
    unsetSession('account');
    unset($account_logged);
    return;
}

}


?>

<script>
    // Simulate an HTTP redirect:
    window.location.replace("<?php echo $redirect_url ?>");
</script>