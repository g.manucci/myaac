<?php

// we need some functions
require '../../common.php';
require SYSTEM . 'functions.php';
require SYSTEM . 'init.php';
require SYSTEM . 'login.php';

function GetCurrentUser(){
    $current_session = getSession('account');
    if($current_session !== false)
    {
        $account_logged = new OTS_Account();
        $account_logged->load($current_session);
        if($account_logged->isLoaded() && $account_logged->getPassword() == getSession('password')
            //&& (!isset($_SESSION['admin']) || admin())
            && (getSession('remember_me') !== false || getSession('last_visit') > time() - 15 * 60)) {  // login for 15 minutes if "remember me" is not used
                $logged = true;
                return $account_logged;
        }
        else {
            unsetSession('account');
            unset($account_logged);
        }

    }
    return null;
}

function GetUserInfo($token) {
    $result = TwitchCurl("https://api.twitch.tv/helix/users", $token);
    return $result;
} 

function GetUserSubscriptions($userId, $token, $broadcaster) {
    $result = TwitchCurl("https://api.twitch.tv/helix/subscriptions/user?user_id=$userId&broadcaster_id=$broadcaster", $token);
    return $result;
} 

function AddTibiaCoins($t_coins)
{
    $currUser = GetCurrentUser();
    $oldCount = $currUser->getCustomField('coins');
    $currUser->setCustomField('coins', $t_coins + $oldCount);
    $currUser->logAction("Added " . $t_coins . " coins to the account.");
}

function TwitchCurl($url, $token) {
    // create curl resource
    $ch = curl_init();

    // set url
    curl_setopt($ch, CURLOPT_URL, $url);
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Client-Id: kc12r81fcalrx9mngd44fw6oz1vfqi',
    'Authorization: Bearer ' . $token
    ));

    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // $output contains the output string
    $output = curl_exec($ch);
    
    $obj = json_decode($output);
    
    $obj->statuscode = curl_getinfo($ch)["http_code"];
    
    // close curl resource to free up system resources
    curl_close($ch);    

    return $obj;
}

function ValidateSubscription($tokenValue, $state)
{
    global $db;

    $current_user = GetCurrentUser();
    
    if ($current_user->getCustomField('twitch_state') != $state)
    {
        return false;
    }
    
    $broadcaster = 234371833; //234371833; //181077473 //Gaules;


    $result = GetUserInfo($tokenValue);

    if ($result->statuscode != 200){
        $user_id = "null";
        $current_user->logAction("Tried to link account to twitch but user id (twitch) wasn't found." );
        return false;
    }
    else{
        $user_id = $result->data[0]->id;
    }

    $query = $db->query('SELECT count(`twitch_account`) as `how_much` FROM `accounts` WHERE `twitch_account` = ' . $user_id . ';');
    $query = $query->fetch();
    $total_accounts = $query['how_much'];

    if ($total_accounts > 0){
        echo "duplicate";
        $current_user->logAction("Tried to link account to twitch but id is already taken." );
        return false;
    }

    $subscriptions = GetUserSubscriptions($user_id, $tokenValue, $broadcaster);

    // echo $subscriptions->data[0]->broadcaster_id;
    // echo $user_id;

    if ($subscriptions->statuscode != 200){
        
        $subscription = "null";
        return false;
    }
    else
    {
        foreach ($subscriptions->data as $sub)
        {
            // echo $sub->broadcaster_id;

            if ($sub->broadcaster_id == $broadcaster)
            {
                $subscription = $subscriptions->data[0]->tier;
                $broadcaster_id = $subscriptions->data[0]->broadcaster_id;
                $broadcaster_name = $subscriptions->data[0]->broadcaster_name;
                
                
                $current_user->setCustomField('twitch_account', $user_id);
                $current_user->setCustomField('twitch_token', $tokenValue);
                $coinsAmount = 100;
                AddTibiaCoins($coinsAmount);
                $current_user->logAction("Linked account to twitch successfully!");
                return true;
            }
        }
    }

    $current_user->logAction("Tried to link account to twitch but subscription wasn't found." );
    return false;
}